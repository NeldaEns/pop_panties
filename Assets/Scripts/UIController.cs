﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    public static UIController ins;

    [HideInInspector]
    public UIScreenBase currentScreen;
    public GameObject uiCamera;

    private void Awake()
    {
        if (ins != null)
            Destroy(gameObject);
        else
        {
            ins = this;
            DontDestroyOnLoad(gameObject);
            DontDestroyOnLoad(uiCamera);
        }
    }

    //-------------------------------------------------

    private void Start()
    {
        Canvas canvas = gameObject.GetComponent<Canvas>();
        canvas.renderMode = RenderMode.ScreenSpaceCamera;
        canvas.worldCamera = uiCamera.GetComponent<Camera>();

    }
}
