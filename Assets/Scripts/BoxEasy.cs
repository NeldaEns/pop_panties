﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxEasy : MonoBehaviour
{
    public int x;
    public int y;
    public BoxType type;

    Vector3 firstBox = new Vector3(-4.3f, -4.3f);
    float boxSize = 0.9555f;

    public Vector3 CalPos(int x, int y)
    {
        return new Vector3(firstBox.x + boxSize * x, firstBox.y + boxSize * y, 0);
    }

    //-----------------------------

    public void OnSpawn(int _x, int _y, BoxType _type)
    {
        x = _x;
        y = _y;
        type = _type;
    }

    //-------------------------

    public void MoveDown()
    {
        StartCoroutine(MoveCoroutine());
    }

    //--------------------

    public IEnumerator MoveCoroutine()
    {
        for (int i = 0; i < 10; i++)
        {
            transform.position -= new Vector3(0, boxSize / 10, 0);
            yield return new WaitForSeconds(0.01f);
        }
    }

    //-------------------------------------------

    private void OnMouseDown()
    {
        GameController.ins.breakBox.Add(gameObject);
        GameController.ins.FindBreakBoxEasy();
    }
}

public enum BoxType
{
    None = 0,
    Purple,
    Green,
    Orange,
    Red,
    Yellow,
}
