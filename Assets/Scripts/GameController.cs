﻿using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class GameController : MonoBehaviour
{
    public static GameController ins;

    [Header("Box")]
    [SerializeField] private List<List<GameObject>> boxMatrixEasy;

    [SerializeField] private List<GameObject> boxEasy;
    public List<GameObject> breakBox;

    [SerializeField] private GameObject objParent;
    [SerializeField] private GameObject linebroad;



    private void Awake()
    {
        if (ins != null)
            Destroy(ins.gameObject);
        ins = this;
    }

    //---------------------------------------

    private void Start()
    {
        boxMatrixEasy = new List<List<GameObject>>();

        for (int i = 0; i < 10; i++)
        {
            List<GameObject> listBoxEasy = new List<GameObject>();

            for(int j = 0; j < 10; j++)
            {
                listBoxEasy.Add(null);
            }
            boxMatrixEasy.Add(listBoxEasy);
        }


        for(int i = 0; i < 10; i++)
        {
          for(int j = 0; j < 10; j++)
            {
                SpawnBoxEasy(i, j);
                boxMatrixEasy[i][j].transform.SetParent(objParent.transform);
            }
        }
    }

    //-----------------------

    public void SpawnBoxEasy(int x, int y)
    {
        int color = UnityEngine.Random.Range(1, 6);
        GameObject box1 = Instantiate(boxEasy[color - 1]);
        Vector3 pos = box1.GetComponent<BoxEasy>().CalPos(x, y);
        box1.transform.position = pos;
        box1.GetComponent<BoxEasy>().OnSpawn(x, y, (BoxType)color);
        boxMatrixEasy[x][y] = box1;
    }

    //--------------------

    public void FindBreakBoxEasy()
    {
        for(int i = 0; i < breakBox.Count; i++)
        {
            int x = breakBox[i].GetComponent<BoxEasy>().x;
            int y = breakBox[i].GetComponent<BoxEasy>().y;

            if (x > 0 && boxMatrixEasy[x - 1][y] != null && boxMatrixEasy[x - 1][y].GetComponent<BoxEasy>().type == breakBox[i].GetComponent<BoxEasy>().type && !KTBoxEasy(boxMatrixEasy[x - 1][y]))
                breakBox.Add(boxMatrixEasy[x - 1][y]);

            if (x < 9 && boxMatrixEasy[x + 1][y] != null && boxMatrixEasy[x + 1][y].GetComponent<BoxEasy>().type == breakBox[i].GetComponent<BoxEasy>().type && !KTBoxEasy(boxMatrixEasy[x + 1][y]))
                breakBox.Add(boxMatrixEasy[x + 1][y]);

            if (y > 0 && boxMatrixEasy[x][y - 1] != null && boxMatrixEasy[x][y - 1].GetComponent<BoxEasy>().type == breakBox[i].GetComponent<BoxEasy>().type && !KTBoxEasy(boxMatrixEasy[x][y - 1]))
                breakBox.Add(boxMatrixEasy[x][y - 1]);

            if (y < 9 && boxMatrixEasy[x][y + 1] != null && boxMatrixEasy[x][y + 1].GetComponent<BoxEasy>().type == breakBox[i].GetComponent<BoxEasy>().type && !KTBoxEasy(boxMatrixEasy[x][y + 1]))
                breakBox.Add(boxMatrixEasy[x][y + 1]);
        }

        if(breakBox.Count > 1)
        {
            BreakBoxEasy();
            MoveBoxEasy();
        }

        breakBox.Clear();
    }

    //-------------------------

    public void BreakBoxEasy()
    {
        for(int i = 0; i < breakBox.Count; i++)
        {
            Destroy(breakBox[i]);
        }
    }
    
    //-----------------

    public void MoveBoxEasy()
    {
        for(int i = 0; i < breakBox.Count; i++)
        {
            int x = breakBox[i].GetComponent<BoxEasy>().x;
            int y = breakBox[i].GetComponent<BoxEasy>().y;

            for(int j = y; j < 9; j++)
            {
                boxMatrixEasy[x][j] = boxMatrixEasy[x][j + 1];

                if(boxMatrixEasy[x][j] != null)
                {
                    boxMatrixEasy[x][j].GetComponent<BoxEasy>().y--;
                    boxMatrixEasy[x][j].GetComponent<BoxEasy>().MoveDown();
                }
            }

            boxMatrixEasy[x][9] = null;
        }
        
        for(int i = 9; i >= 0; i--)
        {
            for(int j = 9; j >= 0; j--)
            {
                if(boxMatrixEasy[i][j] == null)
                {
                    SpawnBoxEasy(i, j);
                }
            }
        }    
        
    }

    //----------------------------

    public bool KTBoxEasy(GameObject box)
    {
        for(int i = 0; i < breakBox.Count; i++)
        {
            int boxX = box.GetComponent<BoxEasy>().x;
            int boxY = box.GetComponent<BoxEasy>().y;

            int breakBoxX = breakBox[i].GetComponent<BoxEasy>().x;
            int breakBoxY = breakBox[i].GetComponent<BoxEasy>().y;

            if(boxX == breakBoxX && boxY == breakBoxY)
            {
                return true;
            }
        }

        return false;
    }
}
